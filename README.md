A simple application that consumes the REST API exposed by randomuser.me - a free, open-source API for generating random user data. Like Lorem Ipsum, but for people.
The application pre-loads the next page when there are 3 more items to display.
After loading 3 pages the process stops and no more pages are loaded.