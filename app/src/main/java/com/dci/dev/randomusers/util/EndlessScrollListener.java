package com.dci.dev.randomusers.util;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {

    private int mPreviousTotal = 0;
    private int currentPage = 0;
    /**
     * True if we are still waiting for the last set of data to load.
     **/
    private boolean mLoading = true;

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int totalItemCount = recyclerView.getLayoutManager().getItemCount();
        int lastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();

        if (mLoading) {
            if (totalItemCount > mPreviousTotal) {
                mLoading = false;
                mPreviousTotal = totalItemCount;
            }
        }

        /*
         * We should start pre-loading the next page
         * when there are 3 more items to display.
         */
        int visibleThreshold = Constants.VISIBLE_THRESHOLD;
        boolean isAllowedLoadMore = (lastVisibleItem + visibleThreshold) >= totalItemCount;
        if (!mLoading && isAllowedLoadMore) {
            // End has been reached
            if (currentPage < Constants.MAX_REQUESTED_PAGES) {
                onLoadMore();
                mLoading = true;
            }
        }
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void incrementCurrentPage() {
        currentPage++;
    }

    public void setLoading(boolean loading) {
        this.mLoading = loading;
    }

    public abstract void onLoadMore();

    public void reset() {
        mPreviousTotal = 0;
        currentPage = 0;
        mLoading = true;
    }
}