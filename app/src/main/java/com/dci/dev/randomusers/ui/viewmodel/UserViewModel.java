package com.dci.dev.randomusers.ui.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.dci.dev.randomusers.model.User;
import com.dci.dev.randomusers.util.DateTimeUtils;

public class UserViewModel extends ViewModel {

    private ObservableField<String> username = new ObservableField<>();
    private ObservableField<String> userInfo = new ObservableField<>();
    private ObservableField<String> avatarUrl = new ObservableField<>();
    private ObservableBoolean hasEmailAttachment = new ObservableBoolean();
    private ObservableField<String> emailTimestamp = new ObservableField<>();


    public UserViewModel(User user) {

        this.username.set(uppercaseFirstLetter(user.getName().getFirst()) + " " + uppercaseFirstLetter(user.getName().getLast()));
        this.userInfo.set(user.getDob().getAge() + " years old from " + uppercaseFirstLetter(user.getLocation().getState()));
        this.avatarUrl.set(user.getPicture().getMedium());
        this.hasEmailAttachment.set((!TextUtils.isEmpty(username.get()) && (username.get().length() % 2 == 0)));
        this.emailTimestamp.set(DateTimeUtils.getRandomTime());
    }

    public ObservableField<String> getUsername() {
        return username;
    }

    public ObservableField<String> getUserInfo() {
        return userInfo;
    }

    public ObservableField<String> getAvatarUrl() {
        return avatarUrl;
    }

    public ObservableBoolean getHasEmailAttachment() {
        return hasEmailAttachment;
    }

    public ObservableField<String> getEmailTimestamp() {
        return emailTimestamp;
    }

    private String uppercaseFirstLetter(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }

}
