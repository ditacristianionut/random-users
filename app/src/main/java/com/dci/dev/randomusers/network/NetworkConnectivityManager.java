package com.dci.dev.randomusers.network;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.subjects.PublishSubject;

public class NetworkConnectivityManager {

    private static NetworkConnectivityManager INSTANCE;

    private NetworkConnectivityManager() {
        onlineSubject = PublishSubject.create();
    }

    public static NetworkConnectivityManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new NetworkConnectivityManager();
        }
        return INSTANCE;
    }

    private PublishSubject<Boolean> onlineSubject;

    public Observable<Boolean> onlineSignal() {
        return onlineSubject;
    }

    public Observer<Boolean> onlineObserver() {
        return onlineSubject;
    }
}
