package com.dci.dev.randomusers.util;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.dci.dev.randomusers.R;
import com.squareup.picasso.Picasso;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class BindingAdapters {

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String url) {
        Picasso.get().load(url)
                .error(R.drawable.ic_avatar)
                .placeholder(R.drawable.ic_avatar)
                .transform(new CropCircleTransformation())
                .into(view);
    }

    @BindingAdapter("visibleGone")
    public static void setImageVisibility(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

}
