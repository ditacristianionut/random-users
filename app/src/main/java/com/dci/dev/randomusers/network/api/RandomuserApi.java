package com.dci.dev.randomusers.network.api;

import com.dci.dev.randomusers.model.ServerResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RandomuserApi {

    /**
     * Gets a list of random users from server
     * @param page - You can request multiple pages of a seed with the page parameter
     * @param results - the number of results (users) to generate
     * @param seed - allow you to always generate the same set of users
     * @return A Single object that contains the response
     **/
    @GET("/api")
    Single<ServerResponse> getUsersListFromServer(@Query("page") int page,
                                                  @Query("results") int results,
                                                  @Query("seed") String seed);

    String BASE_URL = "https://randomuser.me/";

}
