package com.dci.dev.randomusers.ui.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.dci.dev.randomusers.R;
import com.dci.dev.randomusers.databinding.ActivityMainBinding;
import com.dci.dev.randomusers.databinding.ContentMainBinding;
import com.dci.dev.randomusers.model.User;
import com.dci.dev.randomusers.network.NetworkConnectivityManager;
import com.dci.dev.randomusers.network.NetworkStateReceiver;
import com.dci.dev.randomusers.ui.adapter.UserAdapter;
import com.dci.dev.randomusers.ui.viewmodel.MainActivityViewModel;
import com.dci.dev.randomusers.util.AndroidUtils;
import com.dci.dev.randomusers.util.Constants;
import com.dci.dev.randomusers.util.EndlessScrollListener;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainActivityViewModel.OnGetUsersListener {

    private MainActivityViewModel mViewModel;
    private ActivityMainBinding mDataBinding;

    private UserAdapter mUsersListAdapter;
    private EndlessScrollListener mEndlessScrollListener;
    private DrawerLayout mDrawerLayout;

    private NetworkStateReceiver mNetworkStateReceiver;

    // region Overrides
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        ContentMainBinding mContentMainBinding = mDataBinding.appBarMain.contentMain;

        mViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        mViewModel.setOnGetUsersListener(this);
        mContentMainBinding.setViewModel(mViewModel);

        setupUi(getApplicationContext());

        mNetworkStateReceiver = new NetworkStateReceiver();
        NetworkConnectivityManager.getInstance().onlineSignal().subscribe(new OfflineSubscriber());

        if (!AndroidUtils.isConnected(getApplicationContext())) {
            showSnackbar(mDataBinding.drawerLayout, getString(R.string.snackbar_action), getString(R.string.no_internet));
        }

        subscribeToLiveData();

        mViewModel.getUsers(mEndlessScrollListener.getCurrentPage(), Constants.USERS_PER_REQUEST, Constants.SEED);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register Broadcast receiver for network connectivity state changes
        registerReceiver(mNetworkStateReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Unregister Broadcast receiver for network connectivity state changes
        unregisterReceiver(mNetworkStateReceiver);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                Toast.makeText(getApplicationContext(), R.string.functionality_not_implemented, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_refresh:
                // Handle the refresh action
                if (AndroidUtils.isConnected(getApplicationContext())) {
                    mUsersListAdapter.clearAll();
                    mEndlessScrollListener.reset();
                    mViewModel.getUsers(mEndlessScrollListener.getCurrentPage(), Constants.USERS_PER_REQUEST, Constants.SEED);
                } else {
                    showSnackbar(mDataBinding.drawerLayout, getString(R.string.snackbar_action), getString(R.string.no_internet));
                }
                break;
            case R.id.nav_about:
                // Handle the about action
                break;
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSuccess() {
        mEndlessScrollListener.incrementCurrentPage();
    }

    @Override
    public void onError(String errorMessage) {
        mEndlessScrollListener.setLoading(false);
    }

    // endregion

    // region Private functions
    private void subscribeToLiveData() {
        mViewModel.getUsersListLiveData().observe(this, this::onLiveDataUpdate);
    }

    private void onLiveDataUpdate(List<User> users) {
        mUsersListAdapter.addItems(users);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setupUi(Context context) {

        // region Toolbar
        mDataBinding.appBarMain.toolbar.setTitle("Users");
        setSupportActionBar(mDataBinding.appBarMain.toolbar);
        // endregion

        // region Fab button
        mDataBinding.appBarMain.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSnackbar(mDataBinding.drawerLayout, getString(R.string.snackbar_action), getString(R.string.functionality_not_implemented));
            }
        });
        // endregion

        // region RecyclerView
        RecyclerView mRecyclerView = mDataBinding.appBarMain.contentMain.activityMainRecyclerViewUsers;
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mUsersListAdapter = new UserAdapter();
        mRecyclerView.setAdapter(mUsersListAdapter);
        mEndlessScrollListener = new EndlessScrollListener() {
            @Override
            public void onLoadMore() {
                mViewModel.getUsers(this.getCurrentPage(), Constants.USERS_PER_REQUEST, Constants.SEED);
            }
        };
        mRecyclerView.addOnScrollListener(mEndlessScrollListener);
        // endregion

        // region Drawer menu
        mDrawerLayout = mDataBinding.drawerLayout;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mDataBinding.appBarMain.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        mDataBinding.navView.setNavigationItemSelectedListener(this);
        // endregion
    }

    private void showSnackbar(View view, String action, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction(action, null).show();
    }
    // endregion

    // region Inner classes
    private class OfflineSubscriber implements Observer<Boolean> {

        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onNext(Boolean online) {
            if (!online) {
                showSnackbar(mDataBinding.drawerLayout, getString(R.string.snackbar_action), getString(R.string.no_internet));
            }
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }
    // endregion
}
