package com.dci.dev.randomusers.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dci.dev.randomusers.util.AndroidUtils;

public class NetworkStateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NetworkConnectivityManager.getInstance().onlineObserver().onNext(AndroidUtils.isConnected(context));
    }
}
