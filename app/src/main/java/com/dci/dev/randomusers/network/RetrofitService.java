package com.dci.dev.randomusers.network;

import com.dci.dev.randomusers.network.api.RandomuserApi;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {

    private static volatile RetrofitService INSTANCE = null;
    private RandomuserApi client;

    private RetrofitService() {
        Retrofit retrofit =
                new Retrofit.Builder().baseUrl(RandomuserApi.BASE_URL)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(provideOkHttpClient())
                        .build();
        client = retrofit.create(RandomuserApi.class);
    }

    public static RetrofitService getInstance() {
        if (INSTANCE == null) {
            synchronized (RetrofitService.class) {
                if (INSTANCE == null) {
                    INSTANCE = new RetrofitService();
                }
            }
        }
        return INSTANCE;
    }

    public RandomuserApi provideClient() {
        return client;
    }

    private OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .cache(null)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();
    }

}
