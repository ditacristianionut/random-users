package com.dci.dev.randomusers.ui.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import android.util.Log;

import com.dci.dev.randomusers.R;
import com.dci.dev.randomusers.model.ServerResponse;
import com.dci.dev.randomusers.model.User;
import com.dci.dev.randomusers.network.RetrofitService;
import com.dci.dev.randomusers.util.AndroidUtils;
import com.dci.dev.randomusers.util.Constants;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivityViewModel extends AndroidViewModel {

    private CompositeDisposable disposable;

    private MutableLiveData<List<User>> usersListLiveData = new MutableLiveData<>();
    private ObservableBoolean isLoading = new ObservableBoolean(false);

    private OnGetUsersListener mOnGetUsersListener;

    // region Constructors
    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        disposable = new CompositeDisposable();
    }
    // endregion

    // region Overrides
    @Override
    protected void onCleared() {
        if (disposable != null)
            disposable.dispose();
    }
    // endregion

    // region Getters and Setter
    public MutableLiveData<List<User>> getUsersListLiveData() {
        return usersListLiveData;
    }

    public ObservableBoolean getIsLoading() {
        return isLoading;
    }

    public void setOnGetUsersListener(OnGetUsersListener onGetUsersListener) {
        this.mOnGetUsersListener = onGetUsersListener;
    }

    // endregion

    // region Public functions

    /**
     * Gets a list of random users from server and updates the value of {@code usersListLiveData}
     *
     * @param page    - You can request multiple pages of a seed with the page parameter
     * @param results - no. of items (users) per page
     **/
    public void getUsers(int page, int results, String seed) {
        disposable.add(Observable.just(AndroidUtils.isConnected(getApplication()))
                .filter(isConnected -> isConnected)
                .switchMapSingle(aBoolean -> RetrofitService.getInstance()
                        .provideClient()
                        .getUsersListFromServer(page, results, seed)
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable ->
                        isLoading.set(true))
                .doFinally(() -> isLoading.set(false))
                .map(ServerResponse::getResults)
                .subscribe(users -> {
                            usersListLiveData.setValue(users);
                            mOnGetUsersListener.onSuccess();
                        },
                        throwable -> {
                            mOnGetUsersListener.onError(throwable.getMessage());
                            Log.e(MainActivityViewModel.class.getSimpleName(), getApplication().getString(R.string.server_error_message) + throwable.getMessage());
                        }
                )
        );
    }

    public void refresh() {
        getUsers(0, Constants.USERS_PER_REQUEST, Constants.SEED);
    }
    // endregion

    // region Interfaces
    public interface OnGetUsersListener {
        void onSuccess();

        void onError(String errorMessage);
    }
    // endregion

}
