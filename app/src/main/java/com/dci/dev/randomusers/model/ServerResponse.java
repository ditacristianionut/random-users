package com.dci.dev.randomusers.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServerResponse{

	@SerializedName("results")
	private List<User> results;

	@SerializedName("info")
	private Info info;

	public List<User> getResults() {
		return results;
	}

	public Info getInfo() {
		return info;
	}
}