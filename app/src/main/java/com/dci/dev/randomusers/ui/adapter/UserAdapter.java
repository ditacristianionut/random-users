package com.dci.dev.randomusers.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dci.dev.randomusers.databinding.ItemUserBinding;
import com.dci.dev.randomusers.model.User;
import com.dci.dev.randomusers.ui.viewmodel.UserViewModel;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private final ArrayList<User> users = new ArrayList<>();

    public void addItems(List<User> usersList) {
        users.addAll(usersList);
        this.notifyDataSetChanged();
    }

    public void clearAll() {
        users.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public @NonNull
    UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemUserBinding binding = ItemUserBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(users.get(position));
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ItemUserBinding binding;
        UserViewModel viewModel;

        ViewHolder(ItemUserBinding binding) {
            this(binding.getRoot());
            this.binding = binding;
        }

        ViewHolder(View view) {
            super(view);
        }

        void bind(@NonNull User user) {
            viewModel = new UserViewModel(user);
            binding.setViewModel(viewModel);
            binding.executePendingBindings();
        }
    }
}


