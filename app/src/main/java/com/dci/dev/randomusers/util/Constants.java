package com.dci.dev.randomusers.util;

public class Constants {

    public static final int USERS_PER_REQUEST = 20;
    public static final int MAX_REQUESTED_PAGES = 3;
    public static final int VISIBLE_THRESHOLD = 3;

    public static final String SEED = "abc";

}
